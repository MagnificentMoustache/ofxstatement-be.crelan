# ofxstatement be.crelan

This project provides an ofxstatement plugin for the Belgian bank Crelan's webbanking export to OFX.

Ofxstatement is a tool to convert bank statements to an open standardised OFX format, suitable for importing to GnuCash/Skrooge/others.

## How to export
The export function is part of the search functionality: go to accounts & cards -> advanced search & print (enter your criteria if desired, leave blank for everything) -> Export & print -> export as CSV.
ofxstatement is a tool to convert proprietary bank statement to OFX format, suitable for importing to GnuCash. Plugins for ofxstatement parse a particular proprietary bank statement format and produces common data structure, that is then formatted into an OFX file.

## How to use

:code:`$ ofxstatement convert -t crelan searchMovement.csv output.ofx`

## Installation

Place into your python site packages:
* site-packages/ofxstatement/plugins/crelan.py
* site-packages/ofxstatement_be_crelan-0.0.1.dist-info/entry_points.txt

A pip installation package is under development.

## Credits

Most of this is lifted verbatim from github.com/bfrisque/ofxstatement-be-bnp



